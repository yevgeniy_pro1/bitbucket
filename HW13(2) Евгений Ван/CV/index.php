
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form</title>
</head>
<body>
<form action="cv.php" method="post">
    <h2>Professional Skills</h2>


    <label for="description"> Description: </label>
    <textarea name = "skill[description]" id = "description" > </textarea>

    <br><br>

    <h1>Skill 1</h1>

    <label for = "name" > Name of skill </label>
    <input type = "text" name = "skill[skills][0][name]" id = "name">

    <br><br>

    <label for = "level" > level </label>
    <input type = "number" name = "skill[skills][0][score]" id = "score" id ="score" min = "1" max = "10">

    <br><br>

    <h1>Skill 2</h1>

    <label for = "name" > Name of skill </label>
    <input type = "text" name = "skill[skills][1][name]" id = "name">

    <br><br>

    <label for = "level" > level </label>
    <input type = "number" name = "skill[skills][1][score]" id = "score" min = "1" max = "10">

    <br><br>

    <h1>Skill 3</h1>

    <label for = "name" > Name of skill </label>
    <input type = "text" name = "skill[skills][2][name]" id = "name">

    <br><br>

    <label for = "level" > level </label>
    <input type = "number" name = "skill[skills][2][score]" id = "score" min = "1" max = "10">

    <br><br>

    <h1>Skill 4</h1>

    <label for = "name" > Name of skill </label>
    <input type = "text" name = "skill[skills][3][name]" id = "name">

    <br><br>

    <label for = "level" > level </label>
    <input type = "number" name = "skill[skills][3][score]" id = "score" min = "1" max = "10">

    <br><br>

    <h1>Skill 5</h1>

    <label for = "name" > Name of skill </label>
    <input type = "text" name = "skill[skills][4][name]" id = "name">

    <br><br>

    <label for = "level" > level </label>
    <input type = "number" name = "skill[skills][4][score]" id = "score" min = "1" max = "10">

    <br><br>

    <h1>Experience 1</h1>

    <label for = "start_date" > Start Date </label>
    <input type = "date" name = "work[0][start_date]" id = "start_date">

    <br><br>

    <label for = "end_date" > End Date </label>
    <input type = "date" name = "work[0][end_date]" id = "end_date">

    <br><br>

    <label for = "company" > Company </label>
    <input type = "text" name = "work[0][company]" id = "company">

    <br><br>

    <label for = "position" > Position </label>
    <input type = "text" name = "work[0][position]" id = "position">

    <br><br>

    <label for = "description" > Description </label>
    <textarea name = "work[0][description]" id = "description"></textarea>

    <br><br>

    <h1>Experience 2</h1>

    <label for = "start_date" > Start Date </label>
    <input type = "date" name = "work[1][start_date]" id = "start_date">

    <br><br>

    <label for = "end_date" > End Date </label>
    <input type = "date" name = "work[1][end_date]" id = "end_date">

    <br><br>

    <label for = "company" > Company </label>
    <input type = "text" name = "work[1][company]" id = "company">

    <br><br>

    <label for = "position" > Position </label>
    <input type = "text" name = "work[1][position]" id = "position">

    <br><br>

    <label for = "description" > Description </label>
    <textarea name = "work[1][description]" id = "description"></textarea>

    <br><br>

    <h1>Experience 3</h1>

    <label for = "start_date" > Start Date </label>
    <input type = "date" name = "work[2][start_date]" id = "start_date">

    <br><br>

    <label for = "end_date" > End Date </label>
    <input type = "date" name = "work[2][end_date]" id = "end_date">

    <br><br>

    <label for = "company" > Company </label>
    <input type = "text" name = "work[2][company]" id = "company">

    <br><br>

    <label for = "position" > Position </label>
    <input type = "text" name = "work[2][position]" id = "position">

    <br><br>

    <label for = "description" > Description </label>
    <textarea name = "work[2][description]" id = "description"></textarea>

    <br><br>

    <input type="submit" value="Send">

</form>
</body>
</html>